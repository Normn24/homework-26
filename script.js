const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  }
];

const div = document.querySelector("#root");
const ul = document.createElement("ul");

function correctObject(item, index) {
  const propertiesRequire = ["author", "name", "price"];
  const li = document.createElement('li');

  for (const element of propertiesRequire) {
    if (!item.hasOwnProperty(element)) {
      throw new Error(`Error: Properties "${element}" is not in object №:${index}.`);
    }
  }
  li.textContent = `Author: ${item.author}, Name: ${item.name}, Price: ${item.price}`;
  ul.append(li);
}


books.forEach((book, index) => {
  try {
    correctObject(book, index);
    div.append(ul);
  } catch (e) {
    console.log(e.message);
  }
})
